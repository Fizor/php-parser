CREATE TABLE  `films` (
 `id` INT( 8 ) NOT NULL AUTO_INCREMENT ,
 `category` VARCHAR( 200 ) NOT NULL ,
 `image` VARCHAR( 500 ) NOT NULL ,
 `desc` VARCHAR( 4000 ) NOT NULL ,
 `videocode` VARCHAR( 8000 ) NOT NULL ,
PRIMARY KEY (  `id` )
);